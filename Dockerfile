# Use Python37
FROM python:3.9-slim

# Allow statements and log messages to immediately appear in the Knative logs
ENV PYTHONUNBUFFERED True
# Copy requirements.txt to the docker image and install packages
COPY requirements.txt /
RUN pip install -r requirements.txt
RUN pip install Flask gunicorn requests flask_cors flask_restful webargs google-cloud-bigquery pandas flask_jsonpify werkzeug flask_restplus
# Set the WORKDIR to be the folder
COPY . /app
# Expose port 5000
#EXPOSE 8080
ENV PORT 8080

WORKDIR /app
# Use gunicorn as the entrypoint
#CMD exec gunicorn --bind :$PORT test:app --workers 1 --threads 1 --timeout 60
#CMD exec gunicorn --bind 8080 test:app --workers 1 --threads 1 --timeout 60
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 --timeout 0 main:app

#2021-03-16r1
#2021-03-19r1
#2021-04-05r0
