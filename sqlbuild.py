import re,  urllib.parse

def build():
    wurcs="WURCS=2.0/2,2,1/[h4334h][a4334h-1x_1-4]/1-2/a?-b1"
    #search_results = api.search_sequence_format_wurcs( urllib.parse.quote(wurcs, safe='') )
    #print( urllib.parse.quote(wurcs, safe='') )
    return urllib.parse.quote(wurcs, safe='')

if __name__ == '__main__':
    
    x = build()
    print(x)
