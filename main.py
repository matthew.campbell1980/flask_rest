from flask import Flask, request, jsonify, make_response
from pandas.io.json import json_normalize
import json, re
#from flask_restful import Resource, Api, reqparse

import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property

from flask_restx import Api, Resource, reqparse

from google.cloud import bigquery
from flask_cors import CORS
from flask_jsonpify import jsonpify
#from flask_sslify import SSLify
from webargs import fields
from webargs.flaskparser import use_args
import pandas as pd
import urllib.parse
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser

import os
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = 'flaskglygen.json'

app = Flask(__name__)
api = Api(app, catch_all_404s=True, version='1.0', title='Glycan Structure API',
    description='API to access structures registered in GlyGen and GlyToucan includes physical attribute, protein glycosylation, GTs, and publications',)
CORS(app, supports_credentials=True)
#sslify = SSLify(app)
# argument parsing
parser = reqparse.RequestParser()
parser.add_argument('user_device', choices = ('IOS', 'ANDROID', 'WEB'), help = 'Bad Choice: {error_msg}')
parsertoucan = reqparse.RequestParser()
parsertoucan.add_argument('numbertoucans', action = 'append')
parser.add_argument('uniprot', action = 'append')
parser.add_argument('examples', action = 'append')
parser.add_argument('toucan', action= 'append')
parser.add_argument('accession', action = 'append')
parser.add_argument('wurcs', action= 'append')
parser.add_argument('mass', action = 'append')
parser.add_argument('hexnac', action = 'append')
parser.add_argument('hex', action = 'append')


parserx = reqparse.RequestParser().add_argument('name', help='Specify your name')
toucan_parser = reqparse.RequestParser().add_argument('GlyTouCan', help='Specify GlyToucan Id')
uniprot_parser = reqparse.RequestParser().add_argument('accession', help='Specify UniProt accession')
wurcs_parser = reqparse.RequestParser().add_argument('wurcs', help='Specify WURCS string')
mass_parser = reqparse.RequestParser().add_argument('mass', help='Specify mass')

hexnac_parser = reqparse.RequestParser().add_argument('hexnac', help='Specify hexnac')
hex_parser = reqparse.RequestParser().add_argument('hex', help='Specify hex')

def build(data):
        querybuild = """
            WITH Input AS (
                SELECT
                glytoucan.glytoucan_ac
                FROM
                `unicarbkb.unicarbkb_glytoucan.please`
                WHERE (
        """
        for x, y in data.items():
            query = f"""\nand (
                    SELECT COUNT(1) 
                    FROM UNNEST(composition) AS x 
                    WHERE ( REGEXP_CONTAINS(LOWER(x.residue), r'{x}') and x.count = {y}) 
                ) >= 1"""

            querybuild += query

        #pattern = re.compile('WHERE \\(\nand \\(')
        xxx = re.sub('WHERE \\(\n\n', '', querybuild)
        xxxx = re.sub('\n\s+\nand\s+\\(', '', xxx)

        x = xxxx
        return x

@api.route('/get-proteoglycoforms/<uniprot>')
class PrintGlycoforms(Resource):
    @api.doc(uniprot_parser) 
    @api.doc(params={
                 'uniprot': {'description': 'uniprot accession', 'in': 'query', 'type': 'string'}
    })
    @api.doc(responses={
        200: 'Success',
        400: 'Validation Error'
    })
    def get(self, uniprot):
        results = {} #empty dataframe
        #args = parser.parse_args()
        #uniprot = args['uniprot']
     
        client = bigquery.Client()
        query = """
            WITH INPUT AS (
            SELECT   g.uniprot_canonical_ac as uniprot, glytoucan.glytoucan_ac as glytoucan, g.position as pos,
            p.pmid as pmid --c.subtype.name AS classtype
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(glycoprotein) as g,
            UNNEST(publication) as p
            where g.uniprot_canonical_ac like @uniprot
            group by pos, glytoucan, pos, pmid, uniprot --, classtype
            --order by uniprot, pos, pmid
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """
        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("uniprot", "STRING", uniprot),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route("/get-glycan-structure-gts/<glytoucan_id>")
class PrintToucanGTs(Resource):
    @api.doc(toucan_parser)
    @api.doc(params={
                 'glytoucan': {'description': 'glytoucan accession', 'in': 'query', 'type': 'string'}
    })
    def get(self, glytoucan_id):
        client = bigquery.Client()
        query = """
            WITH INPUT AS (
            SELECT   enz.uniprot_canonical_ac as uniprot, enz.protein_name as protein, enz.tax_name as taxonomy
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(enzyme) as enz
            where glytoucan.glytoucan_ac like @glytoucan_id
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """

        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("glytoucan_id", "STRING", glytoucan_id),
                        ]
                )
        query_res = client.query(query, job_config = job_config)
        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)

############
@api.route("/get-glytoucan-published-glycoforms/<uniprot>")
class PrintGlytoucanGlycoformsPublication(Resource):
    @api.doc(parser=uniprot_parser)
    @api.doc(params={
                 'uniprot': {'description': 'uniprot accession', 'in': 'query', 'type': 'string'}
    })
    def get(self, uniprot):
     
        client = bigquery.Client()
        query = """
            WITH INPUT AS (
            SELECT   g.uniprot_canonical_ac as uniprot, glytoucan.glytoucan_ac as glytoucan, g.position as pos,
            p.pmid as pmid
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(glycoprotein) as g,
            UNNEST(publication) as p
            where g.uniprot_canonical_ac like @uniprot
            group by uniprot, glytoucan, pos, pmid 
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """
        query_res = client.query(query)
        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("uniprot", "STRING", uniprot)
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route('/glycoproteins/<uniprot>')
class PrintGPs(Resource):
    @api.doc(uniprot_parser)
    @api.doc(params={
                 'uniprot': {'description': 'uniprot accession', 'in': 'query', 'type': 'string'}
    })
    def get(self, uniprot):

        print('acc check ' + uniprot)

        client = bigquery.Client()
        query = """
            WITH INPUT AS (
            SELECT g.uniprot_canonical_ac as p
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(glycoprotein) as g
            where g.uniprot_canonical_ac like '@uniprot
            group by p
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """
        query_res = client.query(query)
        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("uniprot", "STRING", uniprot)

                        ]
                )

        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route('/get-sequence-formats/<glytoucan_id>')
class PrintSequenceFormats(Resource):
    @api.doc(toucan_parser)
    @api.doc(params={
                 'glytoucan': {'description': 'glytoucan accession', 'in': 'query', 'type': 'string'}
    })
    def get(self, glytoucan_id):
        client = bigquery.Client()
        query = """
            WITH Input AS (
            SELECT glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac 
            from `unicarbkb.unicarbkb_glytoucan.please`
            where glytoucan.glytoucan_ac like @glytoucan_id
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """

        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("glytoucan_id", "STRING", glytoucan_id),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)
        
        


@api.route('/get-glytoucan-structure-details/<glytoucan_id>')
class PrintGlytoucan(Resource):
    @api.doc(params={
                 'glytoucan': {'description': 'glytoucan accession', 'in': 'query', 'type': 'string'}
    })
    @api.doc(toucan_parser)
    def get(self, glytoucan_id):
        client = bigquery.Client()
        print('testing gcp logs: ' + glytoucan_id)

    
        query = """
            WITH Input AS (
            SELECT glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac, mass as mass, mass_pme as masspme --as json 
            from `unicarbkb.unicarbkb_glytoucan.please`
            where glytoucan.glytoucan_ac like @glytoucan_id
            --group by glytoucan.glytoucan_ac, wurcs, inchi, smiles_isomeric, glycam, iupac, mass, mass_pme 
            )
            SELECT
            --t,
            --TO_JSON_STRING(t) AS json_row
            --TO_JSON_STRING(t, true) AS json_row
            TO_JSON_STRING(t)
            FROM Input AS t;
        """


        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("glytoucan_id", "STRING", glytoucan_id),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)
        

@api.route('/search-sequence-format-wurcs')
class PrintSequenceFormatWURCS(Resource):
    #@api.doc(wurcs_parser)
    @api.doc(params={
                 'wurcs': {'description': 'wurcs', 'in': 'query', 'type': 'string'}
    })
    def post(self):
        #print("got this" + wurcs)
        wurcs = request.args.get('wurcs')
        wurcs = urllib.parse.quote(wurcs, safe='') 
        print("chgecj: " + wurcs)
        client = bigquery.Client()

        query = """
            WITH Input AS (
            SELECT glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac, mass as mass, mass_pme as masspme --as json 
            from `unicarbkb.unicarbkb_glytoucan.please`
            where wurcs like @wurcs
    
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """

        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("wurcs", "STRING", urllib.parse.unquote(wurcs)),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        print(json_obj)
        return make_response(json_obj, 200)


@api.route('/search-mass-glycoprotein/<mass>')
class PrintSearchMassGlycoprotein(Resource):
    @api.doc(mass_parser)
    def get(self, mass):
        client = bigquery.Client()
        massLower = float(mass)-1.0
        massUpper = float(mass)+1.0


        query = """
            WITH Input AS (
            SELECT g.uniprot_canonical_ac, glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac, mass as mass, mass_pme as masspme --as json 
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(glycoprotein) as g
            where mass > @massLower and mass < @massUpper 
            group by g.uniprot_canonical_ac, glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac, mass as mass, mass_pme as masspme --as json 
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
            
        """


        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("massLower", "FLOAT64", massLower),
                        bigquery.ScalarQueryParameter("massUpper", "FLOAT64", massUpper),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route('/search-mass/<mass>')
class PrintMass(Resource):
    @api.doc(mass_parser)

    def get(self, mass):
        client = bigquery.Client()
        massLower = float(mass)-1.0
        massUpper = float(mass)+1.0


        query = """
            WITH Input AS (
            SELECT glytoucan.glytoucan_ac as glytoucan, wurcs as wurcs, inchi as inchi, smiles_isomeric as smiles , glycam as glycam, iupac as iupac, mass as mass, mass_pme as masspme --as json 
            from `unicarbkb.unicarbkb_glytoucan.please`
            where mass > @massLower and mass < @massUpper    
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """


        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("massLower", "FLOAT64", massLower),
                        bigquery.ScalarQueryParameter("massUpper", "FLOAT64", massUpper),
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route('/search-composition')
class PrintComposition(Resource):
    #@api.doc(hex_parser)
    @api.doc(params={
                 'hex': {'description': 'hexose residues', 'in': 'query', 'type': 'int'},
                 'hexnac': {'description': 'hexnac residues', 'in': 'query', 'type': 'int'},
                 'dhex': {'description': 'dhex residues', 'in': 'query', 'type': 'int'},
                 'neuac': {'description': 'neuac residues', 'in': 'query', 'type': 'int'},
                 'neugc': {'description': 'neugc residues', 'in': 'query', 'type': 'int'},
                 's': {'description': 'S', 'in': 'query', 'type': 'int'},
                 'p': {'description': 'P', 'in': 'query', 'type': 'int'},
                 'hexa': {'description': 'hexa residues', 'in': 'query', 'type': 'int'},
                 'hexn': {'description': 'hexn residues', 'in': 'query', 'type': 'int'}                
                 })
    def get(self):
        client = bigquery.Client()

        hexose = request.args.get('hex')
        hexnac = request.args.get('hexnac')
        dhex = request.args.get('dhex')
        p = request.args.get('p')
        s = request.args.get('s')
        neuac = request.args.get('neuac')
        neugc = request.args.get('neugc')
        hexn = request.args.get('hexn')
        hexa = request.args.get('hexa')

        composition_dict = {}
        if request.args.get('hex') is not None:
            composition_dict['hex'] = request.args.get('hex')

        if request.args.get('hexnac') is not None:
            composition_dict['hexnac'] = request.args.get('hexnac')

        if request.args.get('dhex') is not None:
            composition_dict['dhex'] = request.args.get('dhex')

        if request.args.get('p') is not None:
            composition_dict['p'] = request.args.get('p')

        if request.args.get('s') is not None:
            composition_dict['s'] = request.args.get('s')

        if request.args.get('neuac') is not None:
            composition_dict['neuac'] = request.args.get('neuac')
        
        if request.args.get('neugc') is not None:
            composition_dict['neugc'] = request.args.get('neugc')
        
        if request.args.get('hexn') is not None:
            composition_dict['hexn'] = request.args.get('hexn')

        if request.args.get('hexa') is not None:
            composition_dict['hexa'] = request.args.get('hexa')
        

        endQuery = " group by glytoucan.glytoucan_ac ) SELECT TO_JSON_STRING(t) FROM Input AS t;"

        query = build(composition_dict)
        query += endQuery
        #print("check query: " + query)
        job_config = bigquery.QueryJobConfig()
        query_res = client.query(query, job_config = job_config)
        
        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


@api.route("/get-glytoucan-published-uniprot-glycoforms/<glytoucan>")
class PrintGlytoucanUniprotGlycoformsPublication(Resource):
    @api.doc(parser=toucan_parser)
    @api.doc(params={
                 'glytoucan': {'description': 'glytoucan accession', 'in': 'query', 'type': 'string'}
    })
    def get(self, glytoucan):
     
        client = bigquery.Client()
        query = """
            WITH INPUT AS (
            SELECT   g.uniprot_canonical_ac as uniprot, glytoucan.glytoucan_ac as glytoucan, g.position as pos,
            p.pmid as pmid
            from `unicarbkb.unicarbkb_glytoucan.please`,
            UNNEST(glycoprotein) as g,
            UNNEST(publication) as p
            where glytoucan.glytoucan_ac like @glytoucan
            group by  pmid, uniprot, pos, glytoucan
            )
            SELECT
            TO_JSON_STRING(t)
            FROM Input AS t;
        """
        query_res = client.query(query)
        job_config = bigquery.QueryJobConfig(
                query_parameters=[
                        bigquery.ScalarQueryParameter("glytoucan", "STRING", glytoucan)
                        ]
                )
        query_res = client.query(query, job_config = job_config)

        df = query_res.to_dataframe()
        json_obj = df.to_json(orient='records')
        return make_response(json_obj, 200)


#api.add_resource(PrintGlycoforms, '/get-proteoglycoforms') #get glycans on protein and all positions
#api.add_resource(PrintToucanGTs, '/get-glycan-structure-gts')
#api.add_resource(PrintGlytoucanGlycoformsPublication, '/get-glytoucan-published-glycoforms')
#api.add_resource(PrintGPs, '/glycoproteins')
#api.add_resource(PrintGlytoucan, '/get-glytoucan-structure-details')
#api.add_resource(PrintSequenceFormats, '/get-sequence-formats')
#api.add_resource(PrintSequenceFormatWURCS, '/search-sequence-format-wurcs')
#api.add_resource(PrintMass, '/search-mass')
#api.add_resource(PrintComposition, '/search-composition')
#api.add_resource(PrintTest, '/test/<id>')

if __name__ == '__main__':
    #app.run(ssl_context="adhoc", debug=True, port = 1123)
    #app.run(ssl_context="adhoc", host="0.0.0.0", port=8080)
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))